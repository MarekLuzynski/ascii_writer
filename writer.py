from functions import *


def main():
    clear_screen_after_sleep(0)
    logo_printer()
    clear_screen_after_sleep(2)
    version_info_printing()
    main_menu()

main()