from font import Font
import os
import sys
from time import sleep
import memory
import colorama
from colorama import Fore, Back, Style

first_font = Font()
colorama.init(autoreset=True)

def logo_printer():
    print(Fore.GREEN + """                ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄                   
               ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌                  
               ▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀▀▀ ▐░█▀▀▀▀▀▀▀▀▀  ▀▀▀▀█░█▀▀▀▀  ▀▀▀▀█░█▀▀▀▀                   
               ▐░▌       ▐░▌▐░▌          ▐░▌               ▐░▌          ▐░▌                       
               ▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄▄▄ ▐░▌               ▐░▌          ▐░▌                       
               ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░▌               ▐░▌          ▐░▌                       
               ▐░█▀▀▀▀▀▀▀█░▌ ▀▀▀▀▀▀▀▀▀█░▌▐░▌               ▐░▌          ▐░▌                       
               ▐░▌       ▐░▌          ▐░▌▐░▌               ▐░▌          ▐░▌                       
               ▐░▌       ▐░▌ ▄▄▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄▄▄  ▄▄▄▄█░█▄▄▄▄  ▄▄▄▄█░█▄▄▄▄                   
               ▐░▌       ▐░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌                  
                ▀         ▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀                   
                                                                                                  
           ▄         ▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄           
          ▐░▌       ▐░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌          
          ▐░▌       ▐░▌▐░█▀▀▀▀▀▀▀█░▌ ▀▀▀▀█░█▀▀▀▀  ▀▀▀▀█░█▀▀▀▀ ▐░█▀▀▀▀▀▀▀▀▀ ▐░█▀▀▀▀▀▀▀█░▌          
          ▐░▌       ▐░▌▐░▌       ▐░▌     ▐░▌          ▐░▌     ▐░▌          ▐░▌       ▐░▌          
          ▐░▌   ▄   ▐░▌▐░█▄▄▄▄▄▄▄█░▌     ▐░▌          ▐░▌     ▐░█▄▄▄▄▄▄▄▄▄ ▐░█▄▄▄▄▄▄▄█░▌          
          ▐░▌  ▐░▌  ▐░▌▐░░░░░░░░░░░▌     ▐░▌          ▐░▌     ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌          
          ▐░▌ ▐░▌░▌ ▐░▌▐░█▀▀▀▀█░█▀▀      ▐░▌          ▐░▌     ▐░█▀▀▀▀▀▀▀▀▀ ▐░█▀▀▀▀█░█▀▀           
          ▐░▌▐░▌ ▐░▌▐░▌▐░▌     ▐░▌       ▐░▌          ▐░▌     ▐░▌          ▐░▌     ▐░▌            
          ▐░▌░▌   ▐░▐░▌▐░▌      ▐░▌  ▄▄▄▄█░█▄▄▄▄      ▐░▌     ▐░█▄▄▄▄▄▄▄▄▄ ▐░▌      ▐░▌           
          ▐░░▌     ▐░░▌▐░▌       ▐░▌▐░░░░░░░░░░░▌     ▐░▌     ▐░░░░░░░░░░░▌▐░▌       ▐░▌          
           ▀▀       ▀▀  ▀         ▀  ▀▀▀▀▀▀▀▀▀▀▀       ▀       ▀▀▀▀▀▀▀▀▀▀▀  ▀         ▀           
                                                                                                  """)
def clear_screen_after_sleep(sleep_time):
    sleep(sleep_time)
    os.system('cls')

def version_info_printing():
    animate(Fore.BLUE + """
    
                ----------------Ascii_writer version: 0.9---------------by Marecki 2023-----------""")
    clear_screen_after_sleep(1)

def menu_printing():
    print(Fore.BLUE + """
                +--------------------------------------------------------------------------------+
                |   1. Stwórz napis                                                              |
                |   2. Wyświetl pojedynczy znak                                                  |
                |   3. Zmień czcionkę                                                            |
                |   4. Zapisz ostatni tekst do pliku                                             |
                |   5. Wczytaj plik z napisami                                                   |
                |   6. Zamknij program                                                           |
                +--------------------------------------------------------------------------------+
                """)



def close_app():
    clear_screen_after_sleep(1)
    check = input(Fore.RED + """                                       Czy na pewno zamknąć??? t - TAK / n - NIE                                   
    """)
    colorama.init(autoreset=True)

    if check == "t":
        clear_screen_after_sleep(1)
        print(Fore.RED + """
 ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄   ▄▄▄▄▄▄▄▄▄▄   ▄         ▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄ 
▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░▌ ▐░░░░░░░░░░▌ ▐░▌       ▐░▌▐░░░░░░░░░░░▌▐░▌
▐░█▀▀▀▀▀▀▀▀▀ ▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀█░▌▐░▌       ▐░▌▐░█▀▀▀▀▀▀▀▀▀ ▐░▌
▐░▌          ▐░▌       ▐░▌▐░▌       ▐░▌▐░▌       ▐░▌▐░▌       ▐░▌▐░▌       ▐░▌▐░▌          ▐░▌
▐░▌ ▄▄▄▄▄▄▄▄ ▐░▌       ▐░▌▐░▌       ▐░▌▐░▌       ▐░▌▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄▄▄ ▐░▌
▐░▌▐░░░░░░░░▌▐░▌       ▐░▌▐░▌       ▐░▌▐░▌       ▐░▌▐░░░░░░░░░░▌ ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░▌
▐░▌ ▀▀▀▀▀▀█░▌▐░▌       ▐░▌▐░▌       ▐░▌▐░▌       ▐░▌▐░█▀▀▀▀▀▀▀█░▌ ▀▀▀▀█░█▀▀▀▀ ▐░█▀▀▀▀▀▀▀▀▀ ▐░▌
▐░▌       ▐░▌▐░▌       ▐░▌▐░▌       ▐░▌▐░▌       ▐░▌▐░▌       ▐░▌     ▐░▌     ▐░▌           ▀ 
▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄█░▌     ▐░▌     ▐░█▄▄▄▄▄▄▄▄▄  ▄ 
▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░▌ ▐░░░░░░░░░░▌      ▐░▌     ▐░░░░░░░░░░░▌▐░▌
 ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀   ▀▀▀▀▀▀▀▀▀▀        ▀       ▀▀▀▀▀▀▀▀▀▀▀  ▀ 
                                                                                             """)
        quit()
    elif check == "n":
        clear_screen_after_sleep(1)
        main_menu()
    else:
        clear_screen_after_sleep(1)
        print(Fore.RED + """                                       Coś poszło nie tak. Spróbuj ponownie.                               """)


def main_menu():
    menu_printing()
    animate("                    Wybierz dostępną opcje: ")
    choose = int(input())
    if choose == 1:
        create_ascii_text()
        main_menu()
    elif choose == 2:
        single_char_art_printing(first_font.my_chars) # Wyświetlanie tylko jednego wskazanego znaku
        main_menu()
    elif choose == 3:
        main_menu()
        pass
    elif choose == 4:
        save_last_to_file()
        main_menu()
    elif choose == 5:
        load_all_textfile()
        main_menu()
    else:
        close_app()

def create_ascii_text():
    clear_screen_after_sleep(1)
    canceling_last_memory_lines()
    text_to_create = getting_input_text()
    line_by_line_art_printing(text_to_create, first_font.my_chars) # Wyświetlanie połączonej pierwszej listy z macierzy

def canceling_last_memory_lines():
    memory.first_line = ""
    memory.second_line = ""
    memory.third_line = ""
    memory.fourth_line = ""
    memory.fivth_line = ""

def line_by_line_art_printing(text, fonts):
    clear_screen_after_sleep(1)

    for elm in text:
        if elm in fonts:
            memory.first_line += ''.join(fonts[elm][0]) + " "
            memory.second_line += ''.join(fonts[elm][1]) + " "
            memory.third_line += ''.join(fonts[elm][2]) + " "
            memory.fourth_line += ''.join(fonts[elm][3]) + " "
            memory.fivth_line += ''.join(fonts[elm][4]) + " "
        else:
            pass

    colorama.init(autoreset=True)
    print(Fore.WHITE + memory.first_line)
    print(Fore.WHITE + memory.second_line)
    print(Fore.WHITE + memory.third_line)
    print(Fore.WHITE + memory.fourth_line)
    print(Fore.WHITE + memory.fivth_line)

def single_char_art_printing(fonts):
    clear_screen_after_sleep(1)
    elm = getting_input_text()
    if elm in fonts:
        char_art_printing(fonts[elm])

def char_art_printing(char_ascii):
    for x in char_ascii:
        print(*x, sep = "")

def save_last_to_file():
    f = open("my_file.txt", "w")
    lines = [memory.first_line + "\n",
        memory.second_line + "\n",
        memory.third_line + "\n",
        memory.fourth_line + "\n",
        memory.fivth_line + "\n", ]
    f.writelines(lines)
    f.close()
    clear_screen_after_sleep(1)
    animate("                                       Zapisano pomyślnie;)"
            "")

def load_all_textfile():
    clear_screen_after_sleep(1)
    with open("my_file.txt", "r") as f:
        for line in f:
            print(line, end="")


def getting_input_text():
    animate("                   Wpisz swój tekst: ")
    user_text = input()
    return user_text

def animate(text, time=0.01):
    for letter in text:
        print(letter, end="")
        sys.stdout.flush()
        sleep(time)




