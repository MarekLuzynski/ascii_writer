# ASCII writer
> A small python program that simply displays text as ASCII art fonts.

## Table of Contents
* [General Info](#general-information)
* [Technologies Used](#technologies-used)
* [Features](#features)
* [Screenshots](#screenshots)
* [Setup](#setup)
* [Usage](#usage)
* [Project Status](#project-status)
* [Acknowledgements](#acknowledgements)
* [Contact](#contact)


## General Information
The program's task is to take the content from the user and display it as an ASCII art font.
The options will be to export the generated text to a file, and the option to change the available fonts.
The program does not use any external libraries for this task. The aim of the project is to develop skills in python.


## Technologies Used
- Python - version 3.8.1
- colorama


## Features
List the ready features here:
- No external libraries has been used.
- Option to export to file. (ready1)
- Library argparse. (in develop)


## Screenshots
![Example gif](./img/cmd_ASnMFAfEfa.gif)


## Setup
To start program run writer.py file.
WARNING!!! If you can not see colored or animated text, propably you should install colorama module.
You can simply do it by type in your console: pip install colorama


## Usage
Just enter your text after option 1, press enter.
Second option is for trying single font.
Next options are to change fonts, and to save to file.


## Project Status
Project is: _in progress_


## Acknowledgements
- This project was inspired by my friend's idea. Many thanks @Tomasz Grelski!


## Contact
Created by [Marecki](https://www.linkedin.com/in/marekluzynski) - feel free to contact me!
